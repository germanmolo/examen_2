package com.germanmolo.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import database.DataBase;
import database.Producto;
import database.RegistroProductos;

public class MainActivity extends AppCompatActivity {

    private Producto producto;
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup radTipo;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnNuevo;
    private Button btnEditar;

    private RegistroProductos db;
    private Producto savedContact;
    private int fav;
    private long id;
    private int valRadio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCodigo = (EditText)findViewById(R.id.txtCodigo);
        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtMarca = (EditText)findViewById(R.id.txtMarca);
        txtPrecio = (EditText)findViewById(R.id.txtPrecio);
        txtCodigo = (EditText)findViewById(R.id.txtCodigo);
        radTipo = (RadioGroup)findViewById(R.id.radGrupo);

        btnGuardar = (Button)findViewById(R.id.btnGuardar);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnNuevo = (Button)findViewById(R.id.btnNuevo);
        btnEditar = (Button)findViewById(R.id.btnEditar);

        db = new RegistroProductos(MainActivity.this);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = Long.parseLong(txtCodigo.getText().toString());
                boolean flag = false;
                if(txtCodigo.getText().toString().matches("") || txtNombre.getText().toString().matches("") || txtMarca.getText().toString().matches("") || txtPrecio.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Favor de ingresar todos los datos", Toast.LENGTH_SHORT).show();
                } else { //Guardar o modificar
                    try {
                        db.openDataBase();
                        Producto p = db.getProducto(id, 1);
                        if(p != null){
                            if (p.getCodigo() == txtCodigo.getText().toString() || p.getNombre().matches(txtNombre.getText().toString())) {
                                Toast.makeText(MainActivity.this, "El código o el nombre de este producto ya se encuentra registrado. Escriba datos distintos", Toast.LENGTH_SHORT).show();
                            }else flag = true;
                        } else flag = true;
                        
                        if(flag){
                            Producto nProducto = new Producto();
                            nProducto.setCodigo(txtCodigo.getText().toString());
                            nProducto.setNombre(txtNombre.getText().toString());
                            nProducto.setMarca(txtMarca.getText().toString());
                            nProducto.setPrecio(txtPrecio.getText().toString());
                            nProducto.setTipo(valRadio);
                            db.insertarProducto(nProducto);
                            Toast.makeText(MainActivity.this, "Se agregó el producto con el código " + txtCodigo.getText().toString(), Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                        db.closeDataBase();
                    }catch (Exception e) {
                        System.out.println("Error " + e.getMessage());
                    }
                }
            }
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ProductoActivity.class);
                startActivityForResult(i, 0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "No indicó qué debía de hacer este botón", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void limpiar(){
        txtCodigo.setText("");
        txtNombre.setText("");
        txtPrecio.setText("");
        txtMarca.setText("");
        radTipo.check(R.id.rad_1);
        txtCodigo.requestFocus();
    }

    public void checkedButton(View v){
        int radioId = radTipo.getCheckedRadioButtonId();
        RadioButton radSelected = findViewById(radioId);
        String temp = radSelected.getText().toString();
        if(temp.equals(getString(R.string.rad1))) valRadio = 0;
        else if(temp.equals(getString(R.string.rad2))) valRadio = 1;
    }
}
