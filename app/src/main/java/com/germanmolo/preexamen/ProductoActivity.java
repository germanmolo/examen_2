package com.germanmolo.preexamen;

import android.app.AppComponentFactory;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import database.Producto;
import database.RegistroProductos;

public class ProductoActivity extends AppCompatActivity {

    private Producto producto;
    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioGroup radTipo;
    private Button btnBorrar;
    private Button btnActualizar;
    private Button btnCerrar;
    private Button btnBuscar;

    private RegistroProductos db;
    private Producto savedContact;
    private int fav;
    private long id;
    private long idSel;
    private int valRadio;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.producto_activity);

        txtCodigo = (EditText)findViewById(R.id.txtCodigo);
        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtMarca = (EditText)findViewById(R.id.txtMarca);
        txtPrecio = (EditText)findViewById(R.id.txtPrecio);
        txtCodigo = (EditText)findViewById(R.id.txtCodigo);
        radTipo = (RadioGroup)findViewById(R.id.radGrupo);

        btnBorrar = (Button)findViewById(R.id.btnBorrar);
        btnActualizar = (Button)findViewById(R.id.btnActualizar);
        btnCerrar = (Button)findViewById(R.id.btnCerrar);
        btnBuscar = (Button)findViewById(R.id.btnBuscar);

        limpiar();

        db = new RegistroProductos(ProductoActivity.this);

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = Long.parseLong(txtCodigo.getText().toString());
                if(txtCodigo.getText().toString().matches("")){
                    Toast.makeText(ProductoActivity.this, "Favor de introducir un número", Toast.LENGTH_SHORT).show();
                }else if(db.getProducto(id,1) != null){
                    try{
                        db.openDataBase();
                        db.deleteProducto(id);
                        db.closeDataBase();
                        Toast.makeText(ProductoActivity.this, "Se ha borrado el producto con código " + id, Toast.LENGTH_SHORT).show();
                        limpiar();
                    } catch (Exception e) {
                        // This will catch any exception, because they are all descended from Exception
                        System.out.println("Error " + e.getMessage());
                    }
                }
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = Long.parseLong(txtCodigo.getText().toString());
                boolean flag = false;
                if(txtCodigo.getText().toString().matches("") || txtNombre.getText().toString().matches("") || txtMarca.getText().toString().matches("") || txtPrecio.getText().toString().matches("")) {
                    Toast.makeText(ProductoActivity.this, "Favor de ingresar todos los datos", Toast.LENGTH_SHORT).show();
                } else { //Guardar o modificar
                    db.openDataBase();
                    Producto p = db.getProducto(id,1);
                    if(p != null){
                        if(p.getCodigo() == txtCodigo.getText().toString() || p.getNombre().matches(txtNombre.getText().toString())){
                            if(p.get_ID() != idSel) Toast.makeText(ProductoActivity.this, "El código o el nombre de este producto ya se encuentra registrado. Escriba datos distintos", Toast.LENGTH_SHORT).show();
                            else flag = true;
                        }else flag = true;
                    } else{
                        flag = true;
                    }
                    if (flag) {
                        Producto nProducto = new Producto();
                        nProducto.setCodigo(txtCodigo.getText().toString());
                        nProducto.setNombre(txtNombre.getText().toString());
                        nProducto.setMarca(txtMarca.getText().toString());
                        nProducto.setPrecio(txtPrecio.getText().toString());
                        nProducto.setTipo(valRadio);
                        db.actualizarProducto(nProducto,idSel);
                        Toast.makeText(ProductoActivity.this, "Se actualizó el registro del código " + txtCodigo.getText().toString(), Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                    db.closeDataBase();
                }
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = Long.parseLong(txtCodigo.getText().toString());
                try {
                    db.openDataBase();
                    if(db.getProducto(id,1) != null){
                        Producto p = db.getProducto(id,1);
                        idSel = p.get_ID();
                        txtNombre.setText(p.getNombre());
                        txtMarca.setText(p.getMarca());
                        txtPrecio.setText(p.getPrecio());
                        if(p.getTipo() == 0) radTipo.check(R.id.rad_1);
                        else radTipo.check(R.id.rad_2);
                        db.closeDataBase();
                    }else{
                        Toast.makeText(ProductoActivity.this, "No existe registro de este código", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void limpiar(){
        txtCodigo.setText("");
        txtNombre.setText("");
        txtPrecio.setText("");
        txtMarca.setText("");
        radTipo.check(R.id.rad_1);
        txtCodigo.requestFocus();
    }

    public void checkedButton(View v){
        int radioId = radTipo.getCheckedRadioButtonId();
        RadioButton radSelected = findViewById(radioId);
        String temp = radSelected.getText().toString();
        if(temp.equals(getString(R.string.rad1))) valRadio = 0;
        else if(temp.equals(getString(R.string.rad2))) valRadio = 1;
    }
}