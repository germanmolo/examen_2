package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;
import java.util.ArrayList;

public class RegistroProductos {
    private Context context;
    private DataBase dataBase;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[]{
            DefinirTabla.Producto._ID,
            DefinirTabla.Producto.CODIGO,
            DefinirTabla.Producto.NOMBRE,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.TIPO
    };

    public RegistroProductos(Context context) {
        this.context = context;
        dataBase = new DataBase(this.context);
    }

    public void openDataBase(){
        db= dataBase.getWritableDatabase();
    }

    public long insertarProducto (Producto c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Producto.CODIGO, c.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Producto.MARCA, c.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, c.getPrecio());
        values.put(DefinirTabla.Producto.TIPO, c.getTipo());

        return db.insert(DefinirTabla.Producto.TABLA_NAME, null,values);
    }

    public long actualizarProducto (Producto c, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Producto.CODIGO, c.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Producto.MARCA, c.getMarca());
        values.put(DefinirTabla.Producto.PRECIO, c.getPrecio());
        values.put(DefinirTabla.Producto.TIPO, c.getTipo());

        String criterio = DefinirTabla.Producto._ID + " = " + id;
        return db.update(DefinirTabla.Producto.TABLA_NAME, values,criterio, null);
    }

    public int deleteProducto (long id){
        String criterio = DefinirTabla.Producto.CODIGO + " = " + id;

        return db.delete(DefinirTabla.Producto.TABLA_NAME, criterio, null);
    }

    public Producto readProducto(Cursor cursor){
        Producto c = new Producto();
        c.set_ID(cursor.getInt(0));
        c.setCodigo(cursor.getString(1));
        c.setNombre(cursor.getString(2));
        c.setMarca(cursor.getString(3));
        c.setPrecio(cursor.getString(4));
        c.setTipo(cursor.getInt(5));
        return c;
    }

    public Producto getProducto(long id, int sel){
        Producto producto = null;
        SQLiteDatabase db = dataBase.getReadableDatabase();
        Cursor c;
        if(sel == 0) c= db.query(DefinirTabla.Producto.TABLA_NAME, columnToRead, DefinirTabla.Producto._ID + " = ? ", new String[] {String.valueOf(id)}, null,null,null);
        else c= db.query(DefinirTabla.Producto.TABLA_NAME, columnToRead, DefinirTabla.Producto.CODIGO + " = ? ", new String[] {String.valueOf(id)}, null,null,null);

        if(c.moveToFirst()) producto = readProducto(c);
        c.close();
        return producto;
    }

//    public ArrayList<Producto> allProductos(){
//        ArrayList<Producto> contactos = new ArrayList<Producto>();
//        Cursor cursor = db.query(DefinirTabla.Producto.TABLA_NAME, null,null,null,null,null,null);
//        cursor.moveToFirst();
//        while(!cursor.isAfterLast()){
//            Producto c = readContacto(cursor);
//            contactos.add(c);
//            cursor.moveToNext();
//        }
//        cursor.close();
//        return contactos;
//    }

    public void closeDataBase(){ dataBase.close(); }

}